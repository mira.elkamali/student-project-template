Multimodal Coach Bridge
=============

Prérequis
---------
- Nodemon (https://www.npmjs.com/package//nodemon)
- Node.js
- Angular


Installation
------------

Open the Command Line Interface in both paths (/HannaBot Angular Interface & /MultiModal Bridge) and run the following command to download dependencies :

    npm install


Before running the code in order to ensure connection with dialogflow, you need to add the google application credential in the environment variables. The file is uploaded seperatly, called hannabot-service-account.json
to do that:
   For windows CMD run :
   `set GOOGLE_APPLICATION_CREDENTIALS=KEY_PATH`
   Linux or MacOS (example):
   `export GOOGLE_APPLICATION_CREDENTIALS="/home/user/Downloads/hannabot-service-account.json"`

   (for more details https://cloud.google.com/dialogflow/es/docs/quick/setup)

Running the code
---------

To start the backend server run

    nodemon index.js

To start the interface run

    ng serve -o


Google account credentials
--------

email: hannachatbot@gmail.com
password: Hanna@chatbot
