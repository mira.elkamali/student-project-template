import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

 SOCKET_ENDPOINT = 'https://multimodalbridge-nestore.isc.heia-fr.ch:3000';
// SOCKET_ENDPOINT = 'http://localhost:3000';

  constructor() { }

}
