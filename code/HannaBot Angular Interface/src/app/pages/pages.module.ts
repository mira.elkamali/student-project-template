import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'text',
    pathMatch: 'full'
  },
  {
    path: 'chatbot',
    loadChildren: () => import('./chatbot/chatbot.module').then(m => m.ChatbotModule)
  },
  {
    path: 'text',
    loadChildren: () => import('./text/text.module').then(m => m.TextModule)
  },
  {
    path: 'voice',
    loadChildren: () => import('./voice/voice.module').then(m => m.VoiceModule)
  },
  {
    path: 'hybrid',
    loadChildren: () => import('./hybrid/hybrid.module').then(m => m.HybridModule)
  },
  {
    path: 'complementary',
    loadChildren: () => import('./complementary/complementary.module').then(m => m.ComplementaryModule)
  },
  {
    path: 'alternate',
    loadChildren: () => import('./alternate/alternate.module').then(m => m.AlternateModule)
  },
  {
    path: 'synergistic',
    loadChildren: () => import('./synergistic/synergistic.module').then(m => m.SynergisticModule)
  },
  {
    path: '**',
    redirectTo: 'text'
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' }), //forRoot(routes, { relativeLinkResolution: 'legacy' })],
  ],
  exports: [RouterModule],
})

export class PagesModule { }
