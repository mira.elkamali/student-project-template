import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlternateComponent } from './alternate.component';
import { AlternateRoutingModule } from './alternate-routing.module';
import { NbChatModule, NbSpinnerModule } from '@nebular/theme';



@NgModule({
  declarations: [
    AlternateComponent
  ],
  imports: [
    CommonModule,
    NbChatModule,
    NbSpinnerModule,
    AlternateRoutingModule,
  ]
})
export class AlternateModule { }
