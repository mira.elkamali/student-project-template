import { Component, OnDestroy, OnInit } from '@angular/core';
import { VoiceRecognitionService } from 'src/app/services/voice-recognition-service.service';
import { AlternateService } from './alternate.service';
const DecibelMeter = require('decibel-meter');

@Component({
  selector: 'app-alternate',
  templateUrl: './alternate.component.html',
  styleUrls: ['./alternate.component.scss']
})
export class AlternateComponent implements OnInit, OnDestroy {

  messages: any[] = [];
  loading = false;

  ChatMsg: string = "";
  isRecording: boolean = false;

  NoiseLevel = 0;
  meter: any;

  constructor(
    private chatService: AlternateService,
    private SpeechService: VoiceRecognitionService
  ) {
    this.SpeechService.init();
    this.meter = new DecibelMeter('mictest');
  }

  ngOnInit(): void {
    this.chatService.connect();

    this.addBotMessage({ text: 'Hello there!', ImageURL: '' }, false, true);

    // Triggered when receiving object from backend server
    this.chatService.getNewMessage().subscribe((data) => {
      if (data != '') {

        if (data.decision == "voice") {
          this.addBotMessage(data, true, false);
        }
        else if (data.decision == "hybrid") {
          this.addBotMessage(data, true, true);
        }
        else {
          this.addBotMessage(data, false, true);
        }
      }
      this.loading = false;
    });

    // this.GetVolume();
  }

  GetVolume() {
    this.meter.sources.then((sources: any[]) => {
      this.meter.connect(sources[0]);
      this.meter.listenTo(0, (dB: any, percent: any, value: any) => this.NoiseLevel = (percent));
    });
  }

  startService() {
    this.ChatMsg = '';
    this.isRecording = true;
    this.SpeechService.start();
  }

  stopService() {
    this.SpeechService.stop();
    this.ChatMsg = this.SpeechService.text;
    this.isRecording = false;
  }

  // Gets device battery level and charging status
  GetBatteryLevel(): Promise<any> {
    return new Promise<number>((resolve, reject) => {
      (window.navigator as any).getBattery().then((res: any) => {
        resolve(res);
      },
      );
    })
  }

  // When user sends a message
  handleUserMessage(event: any) {
    const text = event.message;

    this.addUserMessage(text);
    this.loading = true;


    this.GetVolume();
    console.log(this.NoiseLevel);

    this.GetBatteryLevel().then((res) => {
      var SentObj = {
        message: text,
        battery: res.level * 100,
        is_charging: res.charging,
        noise: this.NoiseLevel
      }

      this.chatService.sendMessage(SentObj);
    });

  }

  // Helpers
  addUserMessage(text: string) {
    this.messages.push({
      text,
      type: 'text',
      sender: 'You',
      reply: true,
      date: new Date()
    });
  }

  addBotMessage(data: any, isVoice: boolean, isDisplayed: boolean) {

    if (isVoice) {

      var objType = "";
      if (data.ImageURL == '') {
        objType = "button";
      }
      else {
        objType = "graphic";
      }

      this.messages.push(
        {
          type: objType,
          customMessageData: {
            text: data.text.replace(/['"]+/g, '').split('\\n').join('\n'),
            isDisplayed,
            isPlaying: false,
            buf: data.Audio,
            media: data.Media,
            url: data.ImageURL,
          },
          reply: false,
          date: new Date(),
          sender: 'NESTORE',
          avatar: '/assets/images/hannabot.png',
        },
      );

    }
    else {
      var objType = "";
      if (data.ImageURL == '') {
        objType = "text";
      }
      else {
        objType = "file";
      }
      this.messages.push({
        media: data.Media,
        url: data.ImageURL,
        text: data.text.replace(/['"]+/g, '').split('\\n').join('\n'),
        type: objType,
        sender: 'NESTORE',
        avatar: '/assets/images/hannabot.png',
        date: new Date()
      });
    }

  }

  playByteArray(customData: any) {
    var arrayBuffer = new ArrayBuffer(customData.buf.byteLength);
    arrayBuffer = customData.buf.slice(0);

    let context = new AudioContext();

    context.decodeAudioData(arrayBuffer).then((buffer) => {

      if (customData.isPlaying) {
        customData.src.stop(0);
      }
      else {
        this.messages.forEach(msg => {
          if (msg.customMessageData !== undefined) {
            if (msg.customMessageData.isPlaying == true) {
              msg.customMessageData.src.stop(0);
            }
          }
        });

        // Create a source node from the buffer
        let context = new AudioContext();
        var source = context.createBufferSource();
        source.buffer = buffer;
        // Connect to the final output node (the speakers)
        source.connect(context.destination);

        // Play immediately
        customData.isPlaying = true;
        customData.src = source;
        source.start(0);

        source.addEventListener(('ended'), () => {
          customData.isPlaying = false;
        });
      }
    })
      .catch((error) => {
        console.log(error); /// error is : DOMException: Unable to decode audio data
      });
  }

  ngOnDestroy() {
    this.chatService.disconnect();
  }

}
