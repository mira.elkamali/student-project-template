import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { CommonService } from 'src/app/services/common.service';
import * as io from 'socket.io-client';

@Injectable({
  providedIn: 'root'
})
export class AlternateService {
  private socket: any;

  public message$: BehaviorSubject<any> = new BehaviorSubject('');

  constructor(private CmSvc: CommonService) {
  }

  // Connect to the socket server
  connect() {
    this.socket = io.io(this.CmSvc.SOCKET_ENDPOINT, { transports: ["websocket"] }).connect();
  }

  // Send object to socket server
  public sendMessage(message: any) {
    this.socket.emit('Message', message);
  }

  // Event triggered when receiving object from socket server
  public getNewMessage = () => {
    this.socket.on('Response', (message: any) => {
      this.message$.next(message);
    });

    return this.message$.asObservable();
  };

  // Disconnect from socket server
  disconnect() {
    if (this.socket) {
      this.socket.disconnect();
    }
  }

}
