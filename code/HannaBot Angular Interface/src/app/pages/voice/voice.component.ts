import { Component, OnDestroy, OnInit } from '@angular/core';
import { VoiceRecognitionService } from 'src/app/services/voice-recognition-service.service';
import { VoiceService } from './voice.service';

@Component({
  selector: 'app-voice',
  templateUrl: './voice.component.html',
  styleUrls: ['./voice.component.scss']
})
export class VoiceComponent implements OnInit, OnDestroy {
  messages: any[] = [];
  loading = false;

  ChatMsg: string = "";
  isRecording: boolean = false;

  constructor(
    private chatService: VoiceService,
    private SpeechService: VoiceRecognitionService
  ) {
    this.SpeechService.init();
  }

  ngOnInit(): void {
    this.chatService.connect();

    this.addBotMessage({ text: 'Hello there!', ImageURL: '' }, false, true);

    // Triggered when receiving object from backend server
    this.chatService.getNewMessage().subscribe((data) => {
      if (data != '') {
        this.addBotMessage(data, true, false);
      }
      this.loading = false;
    });
  }

  startService() {
    this.ChatMsg = '';
    this.isRecording = true;
    this.SpeechService.start();
  }

  stopService() {
    this.SpeechService.stop();
    // console.log(this.SpeechService.text);
    this.ChatMsg = this.SpeechService.text;
    this.isRecording = false;
  }

  playByteArray(customData: any) {
    var arrayBuffer = new ArrayBuffer(customData.buf.byteLength);
    arrayBuffer = customData.buf.slice(0);

    let context = new AudioContext();

    context.decodeAudioData(arrayBuffer).then((buffer) => {

      if (customData.isPlaying) {
        customData.src.stop(0);
      }
      else {
        this.messages.forEach(msg => {
          if (msg.customMessageData !== undefined) {
            if (msg.customMessageData.isPlaying == true) {
              msg.customMessageData.src.stop(0);
            }
          }
        });

        // Create a source node from the buffer
        let context = new AudioContext();
        var source = context.createBufferSource();
        source.buffer = buffer;
        // Connect to the final output node (the speakers)
        source.connect(context.destination);

        // Play immediately
        customData.isPlaying = true;
        customData.src = source;
        source.start(0);

        source.addEventListener(('ended'), () => {
          customData.isPlaying = false;
        });
      }
    })
      .catch((error) => {
        console.log(error); /// error is : DOMException: Unable to decode audio data
      });
  }

  // When user sends a message
  handleUserMessage(event: any) {
    const text = event.message;

    this.addUserMessage(text);
    this.loading = true;

    var SentObj = {
      message: text,
      battery: 50,
      is_charging: false
    }

    this.chatService.sendMessage(SentObj);

  }

  // Helpers
  addUserMessage(text: string) {
    this.messages.push({
      text,
      type: 'text',
      sender: 'You',
      reply: true,
      date: new Date()
    });
  }

  addBotMessage(data: any, isVoice: boolean, isDisplayed: boolean) {
    if (isVoice) {
      this.messages.push(
        {
          type: 'button',
          customMessageData: {
            text: data.text.replace(/['"]+/g, '').split('\\n').join('\n'),
            isDisplayed,
            isPlaying: false,
            buf: data.Audio
          },
          reply: false,
          date: new Date(),
          sender: 'NESTORE',
          avatar: '/assets/images/hannabot.png',
        },
      );
    }
    else {
      this.messages.push({
        text: data.text.replace(/['"]+/g, '').split('\\n').join('\n'),
        type: 'text',
        sender: 'NESTORE',
        avatar: '/assets/images/hannabot.png',
        date: new Date()
      });
    }

  }

  ngOnDestroy() {
    this.chatService.disconnect();
  }
}
