import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VoiceComponent } from './voice.component';
import { VoiceRoutingModule } from './voice-routing.module';
import { NbChatModule, NbSpinnerModule } from '@nebular/theme';



@NgModule({
  declarations: [
    VoiceComponent
  ],
  imports: [
    CommonModule,
    NbChatModule,
    NbSpinnerModule,
    VoiceRoutingModule,
  ]
})
export class VoiceModule { }
