import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChatbotComponent } from './chatbot.component';
import { ChatbotRoutingModule } from './chatbot-routing.module';
import { NbChatModule, NbSpinnerModule } from '@nebular/theme';



@NgModule({
  declarations: [
    ChatbotComponent
  ],
  imports: [
    CommonModule,
    NbChatModule,
    NbSpinnerModule,
    ChatbotRoutingModule,
  ]
})
export class ChatbotModule { }
