import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SynergisticComponent } from './synergistic.component';

const routes: Routes = [
    {
        path: '',
        component: SynergisticComponent,
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class SynergisticRoutingModule { }