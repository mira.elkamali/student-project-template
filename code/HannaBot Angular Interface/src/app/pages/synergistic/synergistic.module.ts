import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SynergisticComponent } from './synergistic.component';
import { SynergisticRoutingModule } from './synergistic-routing.module';
import { NbChatModule, NbSpinnerModule } from '@nebular/theme';



@NgModule({
  declarations: [
    SynergisticComponent
  ],
  imports: [
    CommonModule,
    NbChatModule,
    NbSpinnerModule,
    SynergisticRoutingModule,
  ]
})
export class SynergisticModule { }
