import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HybridComponent } from './hybrid.component';

const routes: Routes = [
    {
        path: '',
        component: HybridComponent,
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class HybridRoutingModule { }