import { Component, OnDestroy, OnInit } from '@angular/core';
import { VoiceRecognitionService } from 'src/app/services/voice-recognition-service.service';
import { HybridService } from './hybrid.service';

@Component({
  selector: 'app-hybrid',
  templateUrl: './hybrid.component.html',
  styleUrls: ['./hybrid.component.scss']
})
export class HybridComponent implements OnInit, OnDestroy {
  messages: any[] = [];
  loading = false;
  ChatMsg: string = "";
  isRecording: boolean = false;
  // Voice_List: SpeechSynthesisVoice[] = [];
  // isTTSSupported: boolean = false;

  constructor(
    private chatService: HybridService,
    private SpeechService: VoiceRecognitionService
  ) {
    this.SpeechService.init();
    // this.InitializeVoices();
  }

  ngOnInit(): void {
    this.chatService.connect();
    var temp = new Uint8Array();
    this.addBotMessage({ text: 'Hello there!', ImageURL: '' }, false, true);

    // Triggered when receiving object from backend server
    this.chatService.getNewMessage().subscribe((data) => {
      if (data != '') {
        // if (this.isTTSSupported == false) {
        //   this.addBotMessage(data.text.replace(/['"]+/g, '').split('\\n').join('\n'), data.Audio, false, true);
        // }
        //this.TextToSpeech(data.text.replace(/['"]+/g, '').split('\\n').join('\n'))
        this.addBotMessage(data, true, true);
      }
      this.loading = false;
    });
  }

  startService() {
    this.ChatMsg = '';
    this.isRecording = true;
    this.SpeechService.start();
  }

  stopService() {
    this.SpeechService.stop();
    // console.log(this.SpeechService.text);
    this.ChatMsg = this.SpeechService.text;
    this.isRecording = false;
  }

  playByteArray(customData: any) {
    var arrayBuffer = new ArrayBuffer(customData.buf.byteLength);
    arrayBuffer = customData.buf.slice(0);

    let context = new AudioContext();

    context.decodeAudioData(arrayBuffer).then((buffer) => {

      if (customData.isPlaying) {
        customData.src.stop(0);
      }
      else {
        this.messages.forEach(msg => {
          if (msg.customMessageData !== undefined) {
            if (msg.customMessageData.isPlaying == true) {
              msg.customMessageData.src.stop(0);
            }
          }
        });

        // Create a source node from the buffer
        let context = new AudioContext();
        var source = context.createBufferSource();
        source.buffer = buffer;
        // Connect to the final output node (the speakers)
        source.connect(context.destination);

        // Play immediately
        customData.isPlaying = true;
        customData.src = source;
        source.start(0);

        source.addEventListener(('ended'), () => {
          customData.isPlaying = false;
        });
      }
    })
      .catch((error) => {
        console.log(error); /// error is : DOMException: Unable to decode audio data
      });
  }

  handleUserMessage(event: any) {
    const text = event.message;

    this.addUserMessage(text);
    this.loading = true;

    var SentObj = {
      message: text,
      battery: 50,
      is_charging: false
    }

    this.chatService.sendMessage(SentObj);
  }

  // Helpers
  addUserMessage(text: string) {
    this.messages.push({
      text,
      type: 'text',
      sender: 'You',
      reply: true,
      date: new Date()
    });
  }

  addBotMessage(data: any, isVoice: boolean, isDisplayed: boolean) {
    if (isVoice) {

      var objType = "";
      if (data.ImageURL == '') {
        objType = "button";
      }
      else {
        objType = "graphic";
      }

      this.messages.push(
        {
          type: objType,
          customMessageData: {
            text: data.text.replace(/['"]+/g, '').split('\\n').join('\n'),
            isDisplayed,
            isPlaying: false,
            buf: data.Audio,
            media: data.Media,
            url: data.ImageURL,
          },
          reply: false,
          date: new Date(),
          sender: 'NESTORE',
          avatar: '/assets/images/hannabot.png',
        },
      );

    }
    else {
      var objType = "";
      if (data.ImageURL == '') {
        objType = "text";
      }
      else {
        objType = "file";
      }
      this.messages.push({
        media: data.Media,
        url: data.ImageURL,
        text: data.text.replace(/['"]+/g, '').split('\\n').join('\n'),
        type: objType,
        sender: 'NESTORE',
        avatar: '/assets/images/hannabot.png',
        date: new Date()
      });
    }

  }

  ngOnDestroy() {
    this.chatService.disconnect();
  }

  // Get list of available voices and check compatibility
  // InitializeVoices() {
  //   if ('speechSynthesis' in window) {
  //     this.isTTSSupported = true;

  //     speechSynthesis.addEventListener("voiceschanged", () => {
  //       this.Voice_List = speechSynthesis.getVoices();
  //       // console.log(this.Voice_List);
  //     });

  //   } else {
  //     this.isTTSSupported = false;
  //     console.log('Text-to-speech not supported.');
  //   }
  // }
  // Convert text to Speech
  // TextToSpeech(text: string) {
  //   if (this.isTTSSupported) {
  //     var utterance = new SpeechSynthesisUtterance(text);
  //     utterance.voice = this.Voice_List[0];
  //     utterance.pitch = 1.5;
  //     utterance.rate = 1.25;
  //     utterance.volume = 1;
  //     speechSynthesis.speak(utterance);
  //   }
  // }

  // Replaying/Pausing voice message
  // ReplayMessage(customData: any) {

  //   if (this.isTTSSupported) {

  //     // if message already playing stop it
  //     if (customData.isPlaying) {
  //       speechSynthesis.cancel();
  //       customData.isPlaying = false;
  //     }
  //     else {
  //       // if any other message is playing stop it to start playing this one
  //       this.messages.forEach(msg => {
  //         if (msg.customMessageData !== undefined) {
  //           if (msg.customMessageData.isPlaying == true) {
  //             speechSynthesis.cancel();
  //             msg.customMessageData.isPlaying = false;
  //           }
  //         }
  //       });

  //       var utterance = new SpeechSynthesisUtterance(customData.text);
  //       console.log(this.Voice_List);
  //       //OPTION 1: Female Voice - Hazel
  //       // utterance.voice = this.Voice_List[1];
  //       // utterance.pitch = 1.5;
  //       // utterance.rate = 1.15;

  //       var engVoices = this.Voice_List.filter(voice => voice.lang == 'en-US');
  //       if (engVoices.length > 1) {
  //         utterance.voice = engVoices[1];
  //         utterance.pitch = 1.2;
  //         utterance.rate = 0.75;
  //       }
  //       else {
  //         utterance.voice = engVoices[0];
  //         utterance.pitch = 1.5;
  //         utterance.rate = 0.65;
  //       }

  //       utterance.volume = 1;
  //       speechSynthesis.speak(utterance);
  //       utterance.addEventListener('start', () => {
  //         customData.isPlaying = true;
  //       });
  //       utterance.addEventListener('end', () => {
  //         customData.isPlaying = false;
  //       });
  //     }

  //   }
  // }
  /** 
   // GetVolume() {

  //   navigator.mediaDevices.getUserMedia({ audio: true, video: false })
  //     .then(function (stream) {
  //       let audioContext = new AudioContext();
  //       let analyser = audioContext.createAnalyser();
  //       let microphone = audioContext.createMediaStreamSource(stream);
  //       let javascriptNode = audioContext.createScriptProcessor(2048, 1, 1);

  //       analyser.smoothingTimeConstant = 0.8;
  //       analyser.fftSize = 1024;

  //       microphone.connect(analyser);
  //       analyser.connect(javascriptNode);
  //       javascriptNode.connect(audioContext.destination);

  //       javascriptNode.onaudioprocess = function () {

  //         var array = new Uint8Array(analyser.frequencyBinCount);
  //         analyser.getByteFrequencyData(array);
  //         var values = 0;

  //         var length = array.length;
  //         for (var i = 0; i < length; i++) {
  //           values += (array[i]);
  //         }

  //         var average = values / length;

  //         console.log(Math.round(average));

  //         return;
  //       }
  //     })
  //     .catch(function (err) {
  //       console.log(err);
  //     });

  // }
    **/


}
