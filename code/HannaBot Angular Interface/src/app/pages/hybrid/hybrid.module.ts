import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NbChatModule, NbSpinnerModule } from '@nebular/theme';
import { HybridRoutingModule } from './hybrid-routing.module';
import { HybridComponent } from './hybrid.component';



@NgModule({
  declarations: [
    HybridComponent
  ],
  imports: [
    CommonModule,
    NbChatModule,
    NbSpinnerModule,
    HybridRoutingModule,
  ]
})
export class HybridModule { }
