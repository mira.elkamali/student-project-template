import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ComplementaryComponent } from './complementary.component';

const routes: Routes = [
  {
    path: '',
    component: ComplementaryComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class ComplementaryRoutingModule { }
