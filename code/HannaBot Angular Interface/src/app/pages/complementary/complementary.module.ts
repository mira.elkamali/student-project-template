import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComplementaryComponent } from './complementary.component';
import { ComplementaryRoutingModule } from './complementary-routing.module';
import { NbChatModule, NbSpinnerModule } from '@nebular/theme';



@NgModule({
  declarations: [
    ComplementaryComponent
  ],
  imports: [
    CommonModule,
    NbChatModule,
    NbSpinnerModule,
    ComplementaryRoutingModule,
  ]
})
export class ComplementaryModule { }
