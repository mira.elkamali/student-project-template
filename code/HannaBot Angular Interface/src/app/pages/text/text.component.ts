import { Component, OnDestroy, OnInit } from '@angular/core';
import { VoiceRecognitionService } from 'src/app/services/voice-recognition-service.service';
import { TextService } from './text.service';

@Component({
  selector: 'app-text',
  templateUrl: './text.component.html',
  styleUrls: ['./text.component.scss']
})
export class TextComponent implements OnInit, OnDestroy {
  messages: any[] = [];
  loading = false;
  
  ChatMsg: string = "";
  isRecording: boolean = false;

  constructor(
    private chatService: TextService,
    private SpeechService: VoiceRecognitionService
  ) {
    this.SpeechService.init();
  }

  ngOnInit(): void {
    this.chatService.connect();

    this.addBotMessage({ text: 'Hello there!', ImageURL: '' }, false, true);

    // Triggered when receiving object from backend server
    this.chatService.getNewMessage().subscribe((data) => {
      if (data != '') {
        this.addBotMessage(data, false, true);
      }
      this.loading = false;
    });
  }

  startService() {
    this.ChatMsg = '';
    this.isRecording = true;
    this.SpeechService.start();
  }

  stopService() {
    this.SpeechService.stop();
    // console.log(this.SpeechService.text);
    this.ChatMsg = this.SpeechService.text;
    this.isRecording = false;
  }

  // When user sends a message
  handleUserMessage(event: any) {
    const text = event.message;

    this.addUserMessage(text);
    this.loading = true;

    var SentObj = {
      message: text,
      battery: 50,
      is_charging: false
    }

    this.chatService.sendMessage(SentObj);

  }

  // Helpers
  addUserMessage(text: string) {
    this.messages.push({
      text,
      type: 'text',
      sender: 'You',
      reply: true,
      date: new Date()
    });
  }

  addBotMessage(data: any, isVoice: boolean, isDisplayed: boolean) {

    this.messages.push({
      text: data.text.replace(/['"]+/g, '').split('\\n').join('\n'),
      type: 'text',
      sender: 'NESTORE',
      avatar: '/assets/images/hannabot.png',
      date: new Date()
    });
  }

  ngOnDestroy() {
    this.chatService.disconnect();
  }
}
