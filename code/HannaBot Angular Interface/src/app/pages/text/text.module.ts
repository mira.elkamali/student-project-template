import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NbChatModule, NbSpinnerModule } from '@nebular/theme';
import { TextRoutingModule } from './text-routing.module';
import { TextComponent } from './text.component';



@NgModule({
  declarations: [
    TextComponent
  ],
  imports: [
    CommonModule,
    NbChatModule,
    NbSpinnerModule,
    TextRoutingModule,
  ]
})
export class TextModule { }
