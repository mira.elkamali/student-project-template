// const { performance } = require('perf_hooks');
//var t0 = performance.now();
var RuleEngine = require('node-rules');

var Decision = {
    TextScore: 0,
    VoiceScore: 0
}

const HardScore = 50;
const MediumScore = 20;
const SoftScore = 10;

//Function to manipulate interface scores
InterfaceScore = function (interface, score) {
    if (interface == 'text') {
        Decision.TextScore += score;
    }
    else {
        Decision.VoiceScore += score;
    }
}

/* Rules Array */
var rules = [
    //#region ----Hard Rules----
    {
        "condition": function (R) {
            R.when(this.Privacy.toLowerCase() === 'private');
        },
        "consequence": function (R) {
            InterfaceScore('text', HardScore);
            this.result = true;
            // this.reason = "Could include reason here";
            R.next();
        }
    },
    {
        "condition": function (R) {
            R.when(this.Intent.toLowerCase() === 'pronounce_intent');
        },
        "consequence": function (R) {
            InterfaceScore('voice', HardScore);
            this.result = true;
            R.next();
        }
    },
    {
        "condition": function (R) {
            R.when(this.Battery < 15 && this.IsCharging == false);
        },
        "consequence": function (R) {
            InterfaceScore('text', HardScore);
            this.result = true;
            R.next();
        }
    },
    {
        "condition": function (R) {
            R.when(this.Media_Type.toLowerCase() === 'graphic');
        },
        "consequence": function (R) {
            InterfaceScore('text', HardScore);
            this.result = true;
            R.next();
        }
    },
    // {
    //     "condition": function (R) {
    //         R.when(this.Noise > 110);
    //     },
    //     "consequence": function (R) {
    //         InterfaceScore('text', HardScore);
    //         this.result = true;
    //         R.next();
    //     }
    // },
    //#endregion
    //#region ----Medium Rules----
    {
        "condition": function (R) {
            R.when(this.Emotion.toLowerCase() === 'sad' && this.Emotion_Confidence > 0.35);
        },
        "consequence": function (R) {
            InterfaceScore('voice', MediumScore);
            this.result = true;
            R.next();
        }
    },
    {
        "condition": function (R) {
            R.when(this.Intent.toLowerCase() === 'unhappy_intent');
        },
        "consequence": function (R) {
            InterfaceScore('voice', HardScore);
            this.result = true;
            R.next();
        }
    },
    {
        "condition": function (R) {
            R.when(this.Intent_Type.toLowerCase() === 'instruction');
        },
        "consequence": function (R) {
            InterfaceScore('text', MediumScore);
            this.result = true;
            R.next();
        }
    },
    {
        "condition": function (R) {
            R.when(this.Intent_Type.toLowerCase() === 'complex');
        },
        "consequence": function (R) {
            InterfaceScore('text', MediumScore);
            this.result = true;
            R.next();
        }
    },
    {
        "condition": function (R) {
            R.when(this.Intent_Type.toLowerCase() === 'monitoring');
        },
        "consequence": function (R) {
            InterfaceScore('voice', MediumScore);
            this.result = true;
            R.next();
        }
    },
    //#endregion
    //#region ----Soft Rules----    
    {
        "condition": function (R) {
            R.when(this.Intent_Type.toLowerCase() === 'recommendation');
        },
        "consequence": function (R) {
            InterfaceScore('text', SoftScore);
            this.result = true;
            R.next();
        }
    },
    {
        "condition": function (R) {
            R.when(this.Intent_Type.toLowerCase() === 'negfeedback');
        },
        "consequence": function (R) {
            InterfaceScore('voice', SoftScore);
            this.result = true;
            R.next();
        }
    },
    //#endregion


    // {
    //     "condition": function (R) {
    //         R.when(this.Emotion === 'Sad' && this.Privacy === 'Public');
    //     },
    //     "consequence": function (R) {
    //         InterfaceScore('voice', 10);
    //         this.result = true;
    //         R.stop();
    //     }
    // }
];

//Rule Engine Execution
var ExecuteRules = (fact) => new Promise((resolve, reject) => {

    /* Creating Rule Engine instance and registering rule */
    var R = new RuleEngine();
    R.register(rules);
    Decision.VoiceScore = 0;
    Decision.TextScore = 0;

    console.log("Executing rules");
    R.execute(fact, function (data) {
        if (data.result) {
            console.log(Decision);
            resolve(Decision);
        }
        else {
            reject("No rules applied.");
        }
        // var t1 = performance.now()
        // console.log("Node rules took " + (t1 - t0) + " milliseconds.")
    });


});

module.exports.ExecuteRules = ExecuteRules;
