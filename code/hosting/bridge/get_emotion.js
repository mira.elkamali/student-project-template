const pd = require('paralleldots');
pd.apiKey = "dra94k37JlZkOjHVMoZxFH8Hsqp1elDqJgZTAuEHKh8";

// Getting Emotion from string
var GetEmotion = (Input) =>
    new Promise((resolve, reject) => {
        pd.emotion(Input, "en")
            .then((response) => {
                // console.log("Get Emotion Response: " + response);
                let EMOTIONS = [];
                if (response == "" || response == null || response == undefined) {
                    let FallBack = { "emotion": "Happy", "value": 0.5 };
                    EMOTIONS.push(FallBack);
                    resolve(EMOTIONS);
                }
                else {
                    const result = JSON.parse(response).emotion;
                    let Angry = { "emotion": "Angry", "value": result.Angry };
                    EMOTIONS.push(Angry);
                    let Sad = { "emotion": "Sad", "value": result.Sad };
                    EMOTIONS.push(Sad);
                    let Fear = { "emotion": "Fear", "value": result.Fear };
                    EMOTIONS.push(Fear);
                    let Happy = { "emotion": "Happy", "value": result.Happy };
                    EMOTIONS.push(Happy);
                    let Excited = { "emotion": "Excited", "value": result.Excited };
                    EMOTIONS.push(Excited);
                    let Bored = { "emotion": "Bored", "value": result.Bored };
                    EMOTIONS.push(Bored);

                    EMOTIONS.sort(function (a, b) {
                        return (b.value) - (a.value);
                    });

                    // console.log("Emotions detected: ");
                    // EMOTIONS.forEach(emo => {
                    //     console.log(JSON.stringify(emo.emotion) + ": " + JSON.stringify(emo.value));
                    // });
                    resolve(EMOTIONS[0]);
                }
            })
            .catch((error) => {
                console.log(error);
                reject(error);
            });
    });

module.exports.GetEmotion = GetEmotion;