let fs = require('fs');
let https = require('https');
let express = require('express');

let server = https.createServer({
    cert: fs.readFileSync('/etc/letsencrypt/live/multimodalbridge-nestore.isc.heia-fr.ch/fullchain.pem'),
    key: fs.readFileSync('/etc/letsencrypt/live/multimodalbridge-nestore.isc.heia-fr.ch/privkey.pem')
}, express());

const io = require('socket.io')(server, {
    cors: {
        origin: '*'
    }
});

server.listen(3000, () => {
    console.log('listening on port 3000');
});

// const express = require('express');
// const app = express();
// const http = require('http').createServer(app);

// const io = require('socket.io')(http, {
//     cors: {
//         origin: '*'
//     }
// });
// http.listen(3000, () => {
//     console.log('listening on port 3000');
// });



var engine = require("./rule_engine");
var komprehend = require("./get_emotion");
var dialogflow = require("./call_dialogflow");

const projectId = 'hannabot-qhyl';
const languageCode = 'en';
const sessionId = Math.random().toString(36).slice(-5);



io.on('connection', function (socket) {
    console.log('New connection:', socket.client.id);

    socket.on('Message', function (input) {
        console.log('Object received: ', input);
        const queries = [input.message];
        Promise.all([
            komprehend.GetEmotion(input.message),
            dialogflow.executeQueries(projectId, sessionId, queries, languageCode)
        ]).then(data => {
            var DetectedEmotion = data[0].emotion;
            var EmotionConfidence = data[0].value;

            var DF_Resp = data[1];
            // console.log("DF Object: " + JSON.stringify(DF_Resp));
            var IntentPrivacy = "";
            var IntentType = "";
            var MediaType = "";
            var comp_text = "";
            var BatteryLevel = input.battery;
            var IsPluggedIn = input.is_charging;
            var NoiseLevel = input.noise;

            if (DF_Resp.queryResult.outputContexts.length > 0) {
                let isPrivate = DF_Resp.queryResult.outputContexts.some((context) => context.name.includes("private") == true);
                if (isPrivate) {
                    IntentPrivacy = "private";
                }
                else {
                    IntentPrivacy = "public";
                }

                let typeInd = DF_Resp.queryResult.outputContexts.findIndex(context => context.name.includes("type_"));
                if (typeInd != -1) {
                    IntentType = DF_Resp.queryResult.outputContexts[typeInd].name.split("type_")[1];
                }
                else {
                    IntentType = "";
                }

                let mediatypeInd = DF_Resp.queryResult.outputContexts.findIndex(context => context.name.includes("media_"));
                if (mediatypeInd != -1) {
                    MediaType = DF_Resp.queryResult.outputContexts[mediatypeInd].name.split("media_")[1];
                }
                else {
                    MediaType = "";
                }
            }
            else {
                IntentPrivacy = "public";
            }

            var I_URL = '';
            var GraphicType = '';
            var DFText = '';

            //check if custom payload           
            if (DF_Resp.queryResult.fulfillmentMessages.length > 0) {
                var MediaObj = DF_Resp.queryResult.fulfillmentMessages.find(res => res.payload != undefined);
                if (MediaObj != undefined) {
                    if (MediaObj.payload.fields.url != undefined) {
                        I_URL = MediaObj.payload.fields.url.stringValue;
                        GraphicType = MediaObj.payload.fields.type.stringValue;
                        if(MediaObj.payload.fields.comp_text){
                            comp_text= MediaObj.payload.fields.comp_text.stringValue;
                        }
                    }
                }
            }
            DFText = DF_Resp.queryResult.fulfillmentText;

            var DetectedIntent = DF_Resp.queryResult.intent.displayName;
            var OutputAudio = DF_Resp.outputAudio;

            if (DetectedEmotion == undefined) {
                DetectedEmotion = "Happy";
            }

            Context = {
                Emotion: DetectedEmotion,
                Emotion_Confidence: EmotionConfidence,
                Privacy: IntentPrivacy,
                Intent: DetectedIntent,
                Battery: BatteryLevel,
                IsCharging: IsPluggedIn,
                Noise: NoiseLevel,
                Intent_Type: IntentType,
                Media_Type: MediaType,

            }
            console.log(Context);

            // ****To call Rule Engine****
            engine.ExecuteRules(Context).then((result) => {
                var modality = "";
                // if (result.TextScore == result.VoiceScore && result.TextScore != 0) {
                //     modality = "hybrid";
                // }
                if (result.TextScore >= result.VoiceScore) {
                    modality = "text";
                }
                else {
                    modality = "voice";
                }
                // modality = "hybrid";
                var Returned_Object = {
                    text: JSON.stringify(DFText),
                    decision: modality,
                    ImageURL: I_URL,
                    comp_text: comp_text,
                    Media: GraphicType,
                    Audio: OutputAudio
                }
                console.log(Returned_Object);
                if (DetectedIntent == "Default Fallback Intent") {
                    if (DetectedEmotion.toLowerCase() == "sad" && EmotionConfidence > 0.6) {
                        Returned_Object.text = "I detected that you are sad, I'm always here by your side.";
                        Returned_Object.decision = "text";
                    }
                    if (DetectedEmotion.toLowerCase() == "happy" && EmotionConfidence > 0.6) {
                        Returned_Object.text = "You sound happy. I'm glad to hear that!";
                    }
                }

                socket.emit('Response', Returned_Object);

            });

        }).catch(function (err) {
            console.log(err);
        });

    });


    socket.on('disconnect', function () {
        console.log('Client disconnected.');
    });


});



